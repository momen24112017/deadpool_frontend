import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NextService } from 'src/app/shared/next.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  public detailsForm: FormGroup;
  next = false;

  constructor(private nextService: NextService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.detailsForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      instagram_id: [''],
    });

    this.nextService.dispatched.subscribe((clicked) => {
      this.next = clicked;
      if (this.detailsForm.valid) {
        this.next = false;
        this.nextService.data.email = this.emailControl.value;
        this.nextService.data.name = this.nameControl.value;
        this.nextService.data.instagram_id = this.nameControl.value;
        this.nextService.sendQuestion().subscribe((done) => {});
        this.nextService.nextStep.next('thanks');
      }
    });
  }

  public get nameControl(): FormControl {
    return <FormControl>this.detailsForm.get('name');
  }
  public get emailControl(): FormControl {
    return <FormControl>this.detailsForm.get('email');
  }
  public get instagramControl(): FormControl {
    return <FormControl>this.detailsForm.get('instagram_id');
  }
}
