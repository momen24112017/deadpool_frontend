import { Component, OnInit } from '@angular/core';
import { NextService } from 'src/app/shared/next.service';

@Component({
  selector: 'app-cover',
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.scss']
})
export class CoverComponent implements OnInit {

  constructor(private nextService: NextService) { }

  ngOnInit(): void {
    this.nextService.nextStep.next('question');
  }

}
