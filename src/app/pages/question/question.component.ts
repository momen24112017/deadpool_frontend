import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { NextService } from 'src/app/shared/next.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  question = '';
  next = false;

  public questionFormControl = new FormControl('', [Validators.required]);

  constructor(private nextService: NextService) { }

  ngOnInit(): void {

    this.nextService.dispatched.subscribe(
      clicked => {
        this.next = clicked;
        if (this.questionFormControl.valid) {
          this.nextService.data.question = this.questionFormControl.value;
          this.nextService.nextStep.next('details');
        }
      }
    )
  }

}
