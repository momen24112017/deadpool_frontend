import { Component, OnInit } from '@angular/core';
import { NextService } from 'src/app/shared/next.service';

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.scss']
})
export class ThanksComponent implements OnInit {

  constructor(public nextService: NextService) { }

  ngOnInit(): void {
  }

}
