import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoverComponent } from './pages/cover/cover.component';
import { DetailsComponent } from './pages/details/details.component';
import { QuestionComponent } from './pages/question/question.component';
import { ThanksComponent } from './pages/thanks/thanks.component';

const routes: Routes = [
  { path: '', component: CoverComponent },
  { path: 'question', component: QuestionComponent },
  { path: 'details', component: DetailsComponent },
  { path: 'thanks', component: ThanksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
