import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NextService {

  public nextStep = new BehaviorSubject<string>(null);
  public dispatched = new Subject<boolean>();

  public data = {
    "name": "",
    "email": "",
    "location": "",
    "question": "",
    "instagram_id": ""
}

  constructor(private http: HttpClient) { }

  emit(val) {
    this.dispatched.next(val);
  }

  sendQuestion(): Observable<any> {
    return this.http.post('http://deadpoolworkart.giraffecode.com/public/api/submit', this.data);
  }




}
