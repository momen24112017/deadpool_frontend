import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NextService } from './shared/next.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'deadpool-week-art';
  step ;
  hide = false;

  constructor(
    private nextService: NextService,
    private router: Router
  ) {}

  onNext(): void{
    this.nextService.data.location = 'not specified';
    this.nextService.nextStep.subscribe(
      step => {
        this.step = step
        if (step === 'thanks') {
          this.hide = true;
        }
      }
    );
    this.nextService.emit(true)
    this.router.navigateByUrl(this.step, { skipLocationChange: true });
  }
}
